from alto.lib.database.DBConnection import DBConnection
from mongoengine import signals
from mongoengine_goodjson import Document, EmbeddedDocument
import datetime

dbm = DBConnection.getMongoEngine()


class AccountSummary(EmbeddedDocument):
    account_id  = dbm.LongField()
    name        = dbm.StringField()


class User(Document):

    account     = dbm.EmbeddedDocumentField(AccountSummary,required=True)
    user_id     = dbm.SequenceField()
    username    = dbm.StringField(unique_with='account.account_id')
    name        = dbm.StringField()
    lastname    = dbm.StringField()
    email       = dbm.StringField()
    password    = dbm.StringField()

    update_at = dbm.DateTimeField()

    @staticmethod
    def update_modified(sender, document):
        document.update_at = datetime.datetime.now()


    meta = {
        'strict': False,
        'collection': "users"
    }

signals.pre_save.connect(User.update_modified,sender=User)

#     @classmethod
#     def pre_save(cls, sender, document, **kwargs):
#         document.updated_at = datetime.datetime.now()

# signals.pre_save.connect(SysService.pre_save, sender=SysService)
