from service.ServiceImp import ServiceImp

from flask import request,Response
from bson.json_util import dumps
import json

class ServiceFacade:

	@staticmethod
	def createResponse(status,message,mimetype="application/json"):
		return Response(
					response='{"status": %s,"message": "%s"}'%(status,message),
					status=status, 
					mimetype=mimetype
		)
		
	@staticmethod
	def newUser():
		user = request.json
		ServiceImp.newUser(user)
		return ServiceFacade.createResponse(200,'User created !!')


	@staticmethod
	def listUsers(param):
		items =  ServiceImp.listUsers(param)
		return Response(
					response= items.to_json(),
					status=200, 
					mimetype="application/json"
		)