from alto.lib.database.DBConnection  import DBConnection
from alto.lib.exceptions.ExceptionApi import ExceptionApi

from service.database.mongodb.models.User import User

import time
import mongoengine
import json

class ServiceImp:
	
	@staticmethod
	def newUser(user):
		user['account'] = {'account_id':1,'name':'Cuenta de pruebas'}
		userModel = User(**user)
		try:
			userModel.save()
		except mongoengine.errors.NotUniqueError as err:
			#print(err.args)
			err = str(err)
			if 'username_1_account.account_id_1' in err:
				err = 'username already exists !!'
			
			raise ExceptionApi(400,err )


	@staticmethod
	def listUsers(param):
		items = User.objects()
		print(items)
		return items


	