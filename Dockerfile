FROM nexus.altoalliance.com:8086/ms-base:0.0.1
#build args
ARG BRANCH_UTIL_LIB=master
ARG BRANCH_UTIL_MODEL=master

WORKDIR /app-run
COPY . /app-run

RUN ash /app-run/sh/install_libs.sh
