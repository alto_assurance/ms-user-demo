# ms-base


# Instalacion Local
### 1.- Instalar `Docker` .

**Instalaci�n OSX**

        brew cask install docker
Luego corre **Docker.app** . Click next. Te pedir� permisos de acceso que debes confirmar. Te aparecer� el icono de una ballena en la barra de tareas has click en �l y espera a que aparezca **"Docker is running"**.   

**Instalaci�n Ubuntu**

        sudo apt install docker.io
        sudo systemctl start docker


### 2.- Obt�n  los proyectos

        git clone git@bitbucket.org/alto_assurance/ms-base.git
		git clone git@bitbucket.org/alto_assurance/ms-memcached.git

### 3.- Configurar conexi�n a Base
Por simplicidad la uri de conexi�n a la base se debe especificar en el archivo `Dockerfile`

		ENV SQLALCHEMY_URI=mysql+pymysql://assurance:d3sarr0ll0@123.123.123.123:3307/custos
		
Para este ejemplo de conexi�n, estoy asumiendo que el servidor MySql es local y para poder accederlo desde el contenedor se debe anexar un alias de IP a la interfaz de red, para ello en la m�quina host debes correr el siguiente comando:

		sudo ifconfig lo0 alias 123.123.123.123/24
		
En caso de que est�s accediendo a un servidor con IP p�blica el paso anterior no es necesario.

### 4.- Crea una imagen docker para cada proyecto

        $ cd ms-base
        $ sudo docker build -t ms-base:v1 .
		
        $ cd ms-memcached
        $ sudo docker build -t ms-memcached:v1 .
        
 Verificar si la imagen se cre� correctamente.

		$ sudo docker images
		REPOSITORY     TAG IMAGE ID     CREATED   SIZE 
		ms-base        v1  349e5abc9fec 5sec ago  343MB
		ms-memcached   v1  349e5abc9fec 5sec ago  343MB
	

### 5.- Crear network com�n para que los microservicios puedan verse entre si

		docker network create memcached-net
	
### 6.- Correr los Microservicios
Debes correr ambas apis indicando la red `memcached-net` y asignando el nombre de `demos-ms-memcached` a micro servicio de mencached.

	
		docker run -d --name demos-ms-memcached --net memcached-net ms-memcached:v1
	
		docker run -it -p 8000:8000  --net memcached-net ms-base:v1


### 7.- Ver la Documentaci�n de la API

		http://localhost:8000/api/v1/ui/

